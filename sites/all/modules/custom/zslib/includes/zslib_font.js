/**
 * @file
 * Javascript functions for font resizing
 */

(function($){

  $(function(){
    var $body = $('body'),
        $page = $('#page'),
        fontSize = parseInt($.cookie('szFontSize')) || 10; //   fontsize = 10||cookies.fontsize;

    function updateSize(){
      $page.css('font-size', fontSize/10 + 'em');
      $.cookie('szFontSize', fontSize);
      $(window).trigger('updateFontSize');
    }

    $('.increaseFont').click(function(){
      fontSize = fontSize + 2.5;
      updateSize();
      return false;
    });
    $('.decreaseFont').click(function(){
      fontSize = fontSize - 2.5;
      fontSize = (fontSize < 10) ? 10 : fontSize;
      updateSize();
      return false;
    });
    // Reset font size
    $(".resetFont").click(function () {
      $page.css('font-size', '1em');
      $.cookie('szFontSize', 10);
      return false;
    });

    updateSize();

  });

})(jQuery);