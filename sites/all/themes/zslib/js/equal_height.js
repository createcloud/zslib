(function($){

	$(function(){
		var $target = $('#pane-contacts'),
			$parent = $target.parents('.panels-flexible-row-inside');

		$target.css('height', $parent.height());

		$(window).on('updateFontSize', function(){
			$target.css('height', 'auto');
			$target.css('height', $parent.height());
		});
	});

})(jQuery);