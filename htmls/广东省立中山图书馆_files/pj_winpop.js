﻿/*------------------------------//
pj_tipswindow右下角的tips小窗口
编码为utf-8
必须在网页中使用pj_tipsWindow.init()函数初始化

//------------------------------*/

//获取元素的纵坐标
function pj_getTop(e){
var offset=e.offsetTop;
if(e.offsetParent!=null) offset+=pj_getTop(e.offsetParent);
return offset;
}


var pj_tipsWindow={};
pj_tipsWindow.id='winpop'; //小窗口的ID
pj_tipsWindow.popMaxH=400; //窗口上升的最大高度
//----------------非配置部分，不要修改-----------------------
pj_tipsWindow.winEle=null;
pj_tipsWindow.popH=0;
pj_tipsWindow.show=null;
pj_tipsWindow.hide=null;
pj_tipsWindow.pt=null;
pj_tipsWindow.winY=null;
pj_tipsWindow.winWidth=0;
pj_tipsWindow.winHeight=0;
pj_tipsWindow.init=function(){
	this.winEle=document.getElementById(this.id);
	this.popH=parseInt(this.winEle.style.height);
	this.winWidth=parseInt(this.winEle.style.width);
	//this.winHeight=parseInt(this.winEle.style.height);
	pj_tipsWindow.tipsPop();
	//document.body.onResize=function(){pj_tipsWindow.resize();}
}


pj_tipsWindow.tipsPop=function(){
		if (this.popH == 0) {             //如果窗口的高度是0
		this.winEle.style.display = "block";  //那么将隐藏的窗口显示出来
		//开始以每0.002秒调用函数changeH("up"),即每0.002秒向上移动一次
		//this.winEle.style.
		this.show = setInterval("pj_tipsWindow.changeH('up')", 2);
	} else {  //否则
		//开始以每0.002秒调用函数changeH("down"),即每0.002秒向下移动一次
		this.hide = setInterval("pj_tipsWindow.changeH('down')", 2);
	}
}


pj_tipsWindow.changeH=function(updown){
	this.popH = parseInt(this.winEle.style.height);
	if(updown=='up'){     //如果这个参数是UP
		if (this.popH <= this.popMaxH) {    //如果转化为数值的高度小于等于150
			this.winEle.style.height = (this.popH + 4).toString() + "px";//高度增加4个象素
		}else{
			clearInterval(this.show);//否则就取消这个函数调用,意思就是如果高度超过100象度了,就不再增长 
			this.winY=pj_getTop(this.winEle);
			this.dScroll();
		}
	}
	else if(updown=='down'){
		if(this.popH >= 4){       //如果这个参数是down
			this.winEle.style.height = (this.popH - 4).toString() + "px";//那么窗口的高度减少4个象素
		}else{         //否则
			clearInterval(this.hide);    //否则就取消这个函数调用,意思就是如果高度小于4个象度的时候,就不再减了
			this.winEle.style.display = "none";  //因为窗口有边框,所以还是可以看见1~2象素没缩进去,这时候就把DIV隐藏掉
		}
	}

}

//滚动
pj_tipsWindow.dScroll=function(){
this.winEle.style.posTop = document.documentElement.scrollTop+this.winY; //移动层
timer=window.setTimeout("pj_tipsWindow.dScroll()",10); //1000ms = 1秒
}


pj_tipsWindow.resize=function(){
	alert(123);
//document.getElementById(this.id).style.display = "none";
//document.getElementById(this.id).style.height  = 0;
	//pj_tipsWindow.tipsPop();
	//this.winEle.style.posTop=document.body.clientHeight-this.popH;
	//}
	
}